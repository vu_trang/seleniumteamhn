package tuan6;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestDatePicker {
	private WebDriver driver;
	private String baseUrl;
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "http://jqueryui.com/";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void test() throws InterruptedException {
		driver.get(baseUrl+"datepicker/");
		//click search box
		//driver.findElement(By.id("datepicker")).click();
		// assume selected date is 15-2-2016
		//#1. type date into box
		driver.switchTo().frame(0);
		driver.findElement(By.cssSelector("input#datepicker.hasDatepicker")).click();//.sendKeys("15-2-2016");
		driver.findElement(By.cssSelector("span.ui-icon.ui-icon-circle-triangle-e")).click();
		//click on date 15
		driver.findElement(By.xpath("//div[@id='ui-datepicker-div']/table/tbody/tr[3]/td[2]/a")).click();
		
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
