package tuan2;


import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;


public class VuThiTrang_bai2 {
  private WebDriver driver;
  private String baseUrl;

  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://www.guru99.com";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testVuThiTrangBai2() throws Exception {
    driver.get(baseUrl + "/");
    driver.findElement(By.linkText("Blog")).click();
    driver.findElement(By.linkText("2")).click();
    driver.findElement(By.linkText("Next")).click();
    driver.findElement(By.linkText("Zephyr")).click();
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    driver.quit();
    
  }

}
