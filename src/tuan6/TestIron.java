package tuan6;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class TestIron {
 
	private WebDriver driver;
	private String baseUrl;
	
  @BeforeTest
  public void beforeTest() {
	  driver = new FirefoxDriver();
	  baseUrl = "http://ironsummitmedia.github.io";
	  driver.manage().window().maximize();
	  //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  driver.get(baseUrl + "/startbootstrap-agency/");
  }

  //check whether logo "Start Bootstrap" is display 
  @Test (priority = 0, enabled = false, groups = {"TC_UI"})
  public void checkLogo() {
	  String textLogo = driver.findElement(By.cssSelector("a.navbar-brand.page-scroll")).getText();
	  if ( textLogo.equals("Start Bootstrap") && driver.findElement(By.cssSelector("a.navbar-brand.page-scroll")).isDisplayed()) {
		  System.out.println("test case 'Check Logo' is passed!");
	  } else {
		  System.out.println("test case 'Check Logo' is failed!");
	  }
  }
  
  //verify Text guide download
  @Test (priority = 3, enabled = false, groups = {"TC_Navigate"})
  public void verifyTextGuideDownload() throws InterruptedException {
	  //click menu Portfolio
	  driver.findElement(By.cssSelector("a[href='#portfolio']")).click();
	  //click escapre
	  driver.findElement(By.cssSelector("a.portfolio-link[href='#portfolioModal5']")).click();
	  //verify text "You can download the PSD template in this portfolio sample item at FreebiesXpress.com."
	  String text_all = driver.findElement(By.xpath("//div[@id='portfolioModal5']/div/div[2]/div/div/div/p[3]")).getText();
	  Assert.assertEquals(text_all, "You can download the PSD template in this portfolio sample item at FreebiesXpress.com.");
	  //close project
	  driver.findElement(By.xpath("//div[5]/div/div[2]/div/div/div/button")).click();
  }
  
  //Test navigation
  @Test (priority = 2, enabled = true, groups = {"TC_Navigate"})
  public void testNavigation() throws InterruptedException {
	  //click menu Portfolio
	  driver.findElement(By.cssSelector("a[href='#portfolio']")).click();
	  //click Startup Framework image
	  driver.findElement(By.xpath("//section[2]/div/div[2]/div[2]/a/div")).click();
	  //click link Startup Framework
	  driver.findElement(By.partialLinkText("Startup")).click();
	 /* //click Categories
	  driver.findElement(By.xpath("//div[@id='page-menu-wrapper']/ul[1]/li[2]/a")).click();
	  Thread.sleep(5000);
	  //click Design menu
	  driver.findElement(By.cssSelector("a[href='http://designmodo.com/design/']")).click();
	  //click Responsive Design menu
	  driver.findElement(By.cssSelector("a[href='http://designmodo.com/design/responsive-design/']")).click();*/
	  //check url
	  Assert.assertEquals(driver.getCurrentUrl(), "http://designmodo.com/design/responsive-design/");
  }
  
  
  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
