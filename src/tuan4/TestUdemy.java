package tuan4;

import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestUdemy {

	private WebDriver driver;
	private String baseUrl;
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "https://www.udemy.com/";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.MILLISECONDS);
	}
	
	@Test
	public void test() throws InterruptedException {
		driver.get(baseUrl +"");
		//find search box and sendkyes
		driver.findElement(By.xpath("//*[@id='udemy']/div[3]/div[1]/div[2]/div[2]/form/div/input")).sendKeys("selenium");
		//click search button
		driver.findElement(By.className("home-search-btn")).click();
		//verify text on page: Search results for "selenium"
		driver.getPageSource().contains("Searach result for 'selenium'");
		//Click on "Show Only Courses On Sale" button.
		driver.findElement(By.xpath("//*[@id='ud_courseimpressiontracker']/div[2]/div/div/div[1]/div[1]/div[2]/div[2]/a")).click();
		Thread.sleep(15000);
		//select Paid at Price
		driver.findElement(By.xpath(".//*[@id='ud_courseimpressiontracker']/div[2]/div/div/div[1]/div[2]/ul/li[1]/ul/li/a/label/span[1]")).click();
		Thread.sleep(15000);
		//select this course "Master in Selenium Automation with simple Python Language"
		driver.findElement(By.xpath("//*[@id='courses']/li[3]/a/div[2]")).click();
		//check text on the page Master in Selenium Automation with simple Python 
		if ( driver.getPageSource().contains("Master in Selenium Automation with simple Python Language")) {
			System.out.println("Course is correct");
		} else {
			System.out.println("Wrong course");
		}
		//check url
		Assert.assertEquals(driver.getCurrentUrl(),"https://www.udemy.com/learn-selenium-automation-in-easy-python-language/");
		Thread.sleep(5000);
		
		//click Take this course
		driver.findElement(By.xpath("//*[@id='udemy']/div[4]/div[3]/div/div/div[2]/div/div[1]/div[1]/div/a")).click();
		Thread.sleep(5000);
		//sign up
		driver.findElement(By.id("id_fullname")).sendKeys("Trang Vu");
		driver.findElement(By.id("id_email")).sendKeys("ttnhdvn@gmail.com");
		driver.findElement(By.id("id_password")).sendKeys("Trang1912");
		//driver.findElement(By.id("id_should_subscribe_to_emails")).clear();
		driver.findElement(By.id("id_should_subscribe_to_emails")).click();
		driver.findElement(By.id("submit-id-submit")).click();
		Thread.sleep(5000);
		//click logo
		driver.findElement(By.xpath("//*[@id='logo-box']/img")).click();
		//get title home and print
		System.out.println(driver.getTitle());	
}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}