package tuan6;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class sangbui_css_ironsummitmedia {
	private WebDriver driver;
	private String baseUrl;
	
	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver","E:/Selenium_eclipse/libs/chromedriver_win32/chromedriver.exe");
		driver = new ChromeDriver(); 
		baseUrl = "http://ironsummitmedia.github.io";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void test() throws InterruptedException {
		driver.get(baseUrl + "/startbootstrap-agency/");		
		// Click on menu "Team"
		driver.findElement(By.cssSelector("a[href*='#team']")).click();
		Thread.sleep(3000);		
		// Check Kay Garland picture is present (just check image present, not sure it's in the correct position).
		WebElement ImageFile = driver.findElement(By.cssSelector("img[src=\"img/team/1.jpg\"]"));        
        Boolean ImagePresent = (Boolean) ((JavascriptExecutor)driver).executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", ImageFile);
        if (!ImagePresent)
        {
             System.out.println("Image not displayed.");
        }
        else
        {
            System.out.println("Image displayed.");
        }
        
        // Check correct position.
        String expected_avatar = "http://ironsummitmedia.github.io/startbootstrap-agency/img/team/1.jpg";
        WebElement position_avatar = driver.findElement(By.cssSelector("#team > div > div:nth-child(2) > div:nth-child(1) > div > img"));
        String current_avatar = position_avatar.getAttribute("src");        
        assertEquals(expected_avatar,  current_avatar);
				
		// Click on "Contact"
		driver.findElement(By.cssSelector("a[href*='#contact']")).click();	
		Thread.sleep(3000);
		// Enter Name, Email, Phone, Message.
		driver.findElement(By.cssSelector("#name")).sendKeys("Sang");
		driver.findElement(By.cssSelector("#email")).sendKeys("sang@sb.com");
		driver.findElement(By.cssSelector("#phone")).sendKeys("123456");
		driver.findElement(By.cssSelector("#message")).sendKeys("Test!");		
		// Wait 6s before tearDown.
		Thread.sleep(4000);
	}

	@After
	public void tearDown() {		
		driver.quit();
	}

}
