package tuan1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class BaseClass {
	private WebDriver driver;
	private String baseUrl;
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void test() {
		driver.get(baseUrl+"");
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
