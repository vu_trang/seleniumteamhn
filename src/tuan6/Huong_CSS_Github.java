package tuan6;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.JavascriptExecutor;

public class Huong_CSS_Github {

	private WebDriver driver;
	
	@Before
	public void setUp() {
		driver = new FirefoxDriver(); // Mo firefox len
		driver.manage().window().maximize(); //Bung to cua so
	}
	
	@Test
	public void test() throws InterruptedException {
//		    1. Go to: http://ironsummitmedia.github.io/startbootstrap-agency/
		driver.get("http://ironsummitmedia.github.io/startbootstrap-agency/");		
		
//			2. Click on menu "TEAM"
		driver.findElement(By.cssSelector("a[href=\"#team\"]")).click();
		Thread.sleep(10000);
		
//			3. Verify Kay Garland's avatar is present. <Cách 1>
/*		if(driver.findElement(By.cssSelector("img[src=\"img/team/1.jpg\"]")).isDisplayed()) {   
			   System.out.println("Displayed");
			   }
			  else {
			   System.err.println("Not displayed");
			   }*/
//		3. Verify Kay Garland's avatar is present. <Cách 2>		
		WebElement ImageFile = driver.findElement(By.cssSelector("img[src=\"img/team/1.jpg\"]"));
        
        boolean ImagePresent = (boolean) ((JavascriptExecutor)driver).executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", ImageFile);
        if (!ImagePresent)
        {
             System.out.println("Image not displayed.");
        }
        else
        {
            System.out.println("Image displayed.");
        }
//			4. Click on menu "CONTACT"
		driver.findElement(By.cssSelector("a[href=\"#contact\"]")).click();
		Thread.sleep(10000);
		
//			5. Enter "YOUR NAME", "YOUR EMAIL", "YOUR PHONE", "YOUR MESSAGE".
		driver.findElement(By.cssSelector("#name")).sendKeys("Lan Huong");
		driver.findElement(By.cssSelector("#email")).sendKeys("testappdaily1@gmail.com");
		driver.findElement(By.cssSelector("#phone")).sendKeys("098226118");
		driver.findElement(By.cssSelector("#message")).sendKeys("Huong Xinh Dep");
//		5.1 Click on button Send Message
		driver.findElement(By.cssSelector("button[type=\"submit\"]")).click();
		Thread.sleep(10000);
		
//			6. EXIT. (Do not click Send button).
		driver.close();
		
	}
	
	
	@After
	public void tearDown() {
	driver.quit(); 
	
	}


}
