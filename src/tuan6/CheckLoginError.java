package tuan6;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CheckLoginError {
	private WebDriver driver;
	private String baseUrl;
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "https://www.abglobal.com";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void test() throws InterruptedException {
		driver.get(baseUrl+"/");
		
		//close popup
		driver.findElement(By.cssSelector("div.ab-arrow-box.ab-tips-menu.ng-scope")).click();
		//click menu
		driver.findElement(By.cssSelector("div.navbar-toggle > div.ab-pull-right.ab-menu-only > span")).click();
		
		Thread.sleep(5000); // just for displaying what webdriver does
		//click account access
		driver.findElement(By.cssSelector("li.toggle-menu.highlighted.btn-account-access > span")).click();
		//click Financial Professionals
		driver.findElement(By.xpath("//div[@class='ab-col-12 subnav ng-scope active']/ul/li[2]/span[@class='ng-binding']")).click();
		//click Advisor Site
		driver.findElement(By.xpath("//div[@class='ng-scope']/div/div[2]/div[4]/a[@class='ng-binding ng-scope']")).click();
		Thread.sleep(2000);
		driver.findElement(By.name("userid")).sendKeys("test");
		driver.findElement(By.name("password")).sendKeys("test");
		Thread.sleep(5000); 
		driver.findElement(By.id("rememberme")).click();
		//click login button
		driver.findElement(By.cssSelector("button.ab-btn.ab-btn-default")).click();
		//thieu kiem tra text dung ko, so sanh voi document
		//Assert.assertTrue(driver.findElement(By.cssSelector("div.ab-error-msg")).isDisplayed());
		String error_msg = driver.findElement(By.cssSelector("div.ab-error-msg")).getText();
		if (driver.findElement(By.cssSelector("div.ab-error-msg")).isDisplayed() && error_msg.equals("")) {
			System.out.println("right");
		}
		//close
		driver.findElement(By.xpath("/html/body/div[2]/div[1]/button/span")).click();
		
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
