package tuan6;

import java.util.concurrent.TimeUnit;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestCss {
	private WebDriver driver;
	private String baseUrl;
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "http://ironsummitmedia.github.io/";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void test() {
		driver.get(baseUrl + "startbootstrap-agency/");
		driver.findElement(By.cssSelector("li > a[href='#team']")).click();
		Assert.assertTrue(driver.findElement(By.cssSelector("div.team-member > img[src='img/team/1.jpg']")).isDisplayed());
		driver.findElement(By.cssSelector("li > a[href='#contact']")).click();
		driver.findElement(By.cssSelector("input#name")).sendKeys("trang");
		driver.findElement(By.cssSelector("input#email")).sendKeys("trang@gmail.com");
		driver.findElement(By.cssSelector("input#phone")).sendKeys("1234567890");
		driver.findElement(By.cssSelector("textarea#message")).sendKeys("test meassage");
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
