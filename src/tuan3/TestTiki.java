package tuan3;

import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;


public class TestTiki {
  private WebDriver driver;
  private String baseUrl;

@BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://tiki.vn";
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void test() throws Exception {
    driver.get(baseUrl + "");
    //check title
   assertEquals(driver.getTitle(),"Mua Hàng Trực Tuyến Uy Tín với Giá Rẻ Hơn tại Tiki.vn");
    //find search box
    driver.findElement(By.xpath("/html/body/header/div/div[1]/div[2]/form/div[1]/input")).sendKeys("mac");
    
    //click search button
    driver.findElement(By.xpath("/html/body/header/div/div[1]/div[2]/form/div[1]/span/button")).click();
    //check text "Kết quả tìm kiếm cho 'mac'"
    assertEquals(driver.findElement(By.cssSelector("h1")).getText(),"Kết quả tìm kiếm cho 'mac'");
    
    driver.findElement(By.xpath("/html/body/div[7]/div/div/div[2]/div[2]/div[4]/ul/li[2]/a")).click();
    //check icon 
    assertTrue(driver.findElement(By.xpath("/html/body/footer/div/div/div[3]/div[2]/div/a[1]/img")).isDisplayed());
   
    //back to home
    driver.findElement(By.xpath("/html/body/header/div/div[1]/div[1]/p/a/i")).click();
  }


@AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
	  //close browser
    driver.quit();

  }

}
