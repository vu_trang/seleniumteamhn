package tuan2;

import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DemoWriteaReview {
  private WebDriver driver;
  private String baseUrl;

  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://addons.mozilla.org";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testDemoWriteaReview() throws Exception {
    driver.get(baseUrl + "/EN-us/firefox");
    driver.findElement(By.id("search-q")).clear();
    driver.findElement(By.id("search-q")).sendKeys("firebug");
    driver.findElement(By.cssSelector("button.search-button")).click();
    driver.findElement(By.id("add-review")).click();
    driver.findElement(By.id("id_username")).clear();
    driver.findElement(By.id("id_username")).sendKeys("trang@gmail.com");
    driver.findElement(By.id("login-submit")).click();
    driver.findElement(By.id("id_password")).clear();
    driver.findElement(By.id("id_password")).sendKeys("Trang1912");
    driver.findElement(By.id("login-submit")).click();
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    driver.quit();
    
  }
}
