package tuan6;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestMedium {
	private WebDriver driver;
	private String baseUrl;
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "https://medium.com";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void test() {
		driver.get(baseUrl+"/");
		//driver.findElement(By.xpath("//div[@id='container']/div[2]/div/nav/div[3]/div[1]/input")).sendKeys("language");
		driver.findElement(By.xpath("//input[@placeholder='Search Medium']")).sendKeys("language");
		driver.findElement(By.xpath("//div[@id='container']/div[2]/div/nav/div[3]/div[1]/input")).sendKeys(Keys.ENTER);
		driver.findElement(By.xpath("//div[@id='container']/div[2]/div/div/div/div[2]/div[1]/nav/ul/li[2]/a")).click();
		driver.findElement(By.xpath("//div[@id='container']/div[2]/div/div/div/div[2]/div[2]/ul/li[1]/div[3]/h3/a")).click();
	//	Assert.assertTrue(driver.findElement(By.xpath("//div[@id='container']/div[2]/div/div/div/div[1]/div/div/div/div[3]/span/button[2]")).isDisplayed());
	//another way to check a button
		Assert.assertTrue(driver.findElement(By.xpath("//button[contains(.,'Follow')]")).isDisplayed());
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
