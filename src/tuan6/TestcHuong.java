package tuan6;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestcHuong {
	private WebDriver driver;
	private String baseUrl;
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void test() throws InterruptedException {
		driver.get(baseUrl); 
		  Thread.sleep(5000);
		  driver.findElement(By.cssSelector("div.ab-arrow-box.ab-tips-menu.ng-scope")).click();
		  Thread.sleep(5000);
		  //Click menu
		  driver.findElement(By.cssSelector("div.ab-pull-left.mnu-icon")).click();
		  Thread.sleep(15000);
		  //Click ng-bind-html="'Account Access'
		  driver.findElement(By.cssSelector("span.ng-binding.ng-scope.ab-active-link")).click();
		  Thread.sleep(5000);
		  //Financial Professionals
		  driver.findElement(By.cssSelector("span.ng-binding")).click();
		  Thread.sleep(5000);
		  driver.findElement(By.cssSelector(" a.ng-binding ng-scope[ng-bind-html='Advisor Site']")).click();
		  Thread.sleep(5000);
		  driver.findElement(By.xpath("//input[placeholder='Username']")).sendKeys("HuongBL");
		  driver.findElement(By.xpath("//input[placeholder='Password']")).sendKeys("12345678");
		  driver.findElement(By.cssSelector("input#rememberme[type='checkbox']")).click();
		  driver.findElement(By.cssSelector("button.ab-btn ab-btn-default[type='submit']")).click();
		  Thread.sleep(5000);
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
