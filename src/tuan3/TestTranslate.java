package tuan3;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestTranslate {
	
	private WebDriver driver;
	private String baseUrl;
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "https://translate.google.com";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.MILLISECONDS);
		
	}
	
	@Test
	public void test() throws InterruptedException {
		driver.get(baseUrl+ "/");
		
		//check url whether "https://translate.google.com/"
		Assert.assertEquals( driver.getCurrentUrl(), "https://translate.google.com/");
		
		//choose soure languge
		driver.findElement(By.id("gt-sl-gms")).click();
		Thread.sleep(10000);

		//Actions action = new Actions(driver);
		//WebElement we = driver.findElement(By.xpath("//div[@id=':2g']/div"));
		//action.moveToElement(we).moveToElement(driver.findElement(By.xpath("//div[@id=':2g']/div"))).build().perform();
		//driver.findElement(By.xpath("//div[@id=':2g']/div")).click();
		driver.findElement(By.id(":2g")).click();
//		WebElement element = drim ver.findElement(By.xpath("//div[@id=':2g']/div"));
//		JavascriptExecutor executor = (JavascriptExecutor)driver;
//		executor.executeScript("document.getElementById(':2g').click();", element);
		//System.out.println("here");
		
		//choose target language
		driver.findElement(By.id("gt-tl-gms")).click();
		Thread.sleep(9000);
		driver.findElement(By.id(":3c")).click();
		
		//type
		driver.findElement(By.id("source")).sendKeys("Nh�");
		Thread.sleep(5000);
		
		//check text "Translations of nh�"
		String text_trans = "";
		text_trans = driver.findElement(By.xpath("/html/body/div[3]/div[2]/form/div[2]/div/div/div[2]/div[4]/div[2]/div[2]/div/div/div[1]/div[1]/div/span")).getText();
		Assert.assertEquals(text_trans, "nh�");
		text_trans = driver.findElement(By.xpath("//div[4]/div[2]/div[2]/div/div/div[1]/div[1]/div")).getText();
		//System.out.println(text_trans);
		Assert.assertEquals(text_trans, "Translations of");
	
		Thread.sleep(5000);
		
		//driver.findElement(By.id("gt-ft-mkt-toolkit")).click();
		driver.findElement(By.linkText("Translator Toolkit")).click();
		//check trang moi "Welcome to Google Translator Toolkit"
		//Assert.assertEquals(driver.findElement(By.xpath("/html/body/div[3]/h3")), "Welcome to Google Translator Toolkit");
		Assert.assertTrue(driver.getPageSource().contains("Welcome to Google Translator Toolkit"));
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
	
}
