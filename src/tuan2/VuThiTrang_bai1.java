package tuan2;

import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;


public class VuThiTrang_bai1 {
  private WebDriver driver;
  private String baseUrl;
 

  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://www.guru99.com";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testVuThiTrangBai1() throws Exception {
    driver.get(baseUrl + "/");
    driver.findElement(By.cssSelector("i.icon-coffee")).click();
    driver.findElement(By.name("gsquery")).clear();
    driver.findElement(By.name("gsquery")).sendKeys("arrays");
    driver.findElement(By.cssSelector("form > input[type=\"image\"]")).click();
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    driver.quit();
  }

 
}
