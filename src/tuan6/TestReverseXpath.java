package tuan6;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestReverseXpath {
	private WebDriver driver;
	private String baseUrl;
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "http://www.reverso.net/";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void test() throws InterruptedException {
		driver.get(baseUrl + "spell-checker/english-spelling-grammar/");
		driver.findElement(By.xpath("//form/div/textarea[@id='startText']")).sendKeys("hello, my name are John");
		driver.findElement(By.xpath("//a[@id='btnSpell']")).click();
		Thread.sleep(15000);
		//click logo
		driver.findElement(By.xpath("//div[@id='reversoLogo']/a/img")).click();
		Thread.sleep(10000);
		//driver.findElement(By.linkText("Dictionary")).click();
		driver.findElement(By.xpath("//div[@id='mBar']/div/a[@href='http://dictionary.reverso.net']/span")).click();
		// type book
		driver.findElement(By.xpath("//input[@id='ctl00_cC_txtSearchText']")).sendKeys("book");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@id='ctl00_cC_lnkSearch']")).click();
		Assert.assertEquals(driver.getCurrentUrl(),"http://dictionary.reverso.net/english-definition/");
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
