package tuan3;

import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestReverseEnglish {
  private WebDriver driver;
  private String baseUrl;

@BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://www.reverso.net/spell-checker/english-spelling-grammar";
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void test() throws Exception {
    driver.get(baseUrl + "/");
 
  
    driver.findElement(By.id("startText")).sendKeys("Hello my name are Bill");
    driver.findElement(By.id("btnSpell")).click();  
  }


@AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
	  //close browser
    driver.quit();

  }

}
