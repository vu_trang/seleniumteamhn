package tuan3;


import org.testng.annotations.Test;
import org.testng.annotations.*;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;


public class TestIE {
  private WebDriver driver;
  private String baseUrl;

@BeforeClass(alwaysRun = true)
  public void setUp() {	
	
	DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
	capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
	File file = new File("E:/Selenium/lib/IEDriverServer_x64_2.48.0/IEDriverServer.exe");
	System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
	WebDriver driver = new InternetExplorerDriver(capabilities);
	driver.get("https://www.google.com/");	
   
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void test() {
	
    driver.get(baseUrl + "/");
    //check 
    //driver.findElement(By.id("source")).sendKeys("bonjour!");
   // driver.findElement(By.id("gt-submit")).click();
  }


@AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
	  //close browser
    driver.quit();

  }

}
