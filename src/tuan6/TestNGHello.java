package tuan6;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestNGHello {
	private WebDriver driver;
	private String baseUrl;
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "https://translate.google.com/";
		driver.get(baseUrl + "#vi/en");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test (priority = 0)
	public void test01() {
		//driver.get(baseUrl + "#vi/en");
		driver.findElement(By.cssSelector("textarea#source")).sendKeys("happy new year");
		driver.findElement(By.cssSelector("input#gt-submit")).click();	
	}
	
	@Test (priority = 3, enabled =true)
	public void test02() {
		//driver.get(baseUrl + "#vi/en");
		driver.findElement(By.cssSelector("textarea#source")).sendKeys("happy");
		driver.findElement(By.cssSelector("input#gt-submit")).click();	
	}
	
	@Test (priority = 2, enabled = false, groups = "nameGroup1")
	public void test03() {
		//driver.get(baseUrl + "#vi/en");
		driver.findElement(By.cssSelector("textarea#source")).sendKeys("happy");
		driver.findElement(By.cssSelector("input#gt-submit")).click();	
	}
	
	@Test (priority = 1, groups = "nameGroup1")
	public void test04() {
		//driver.get(baseUrl + "#vi/en");
		driver.findElement(By.cssSelector("textarea#source")).sendKeys("happy");
		driver.findElement(By.cssSelector("input#gt-submit")).click();	
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
