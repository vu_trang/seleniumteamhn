package tuan6;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Ngan_testcase_18_01 {
	private WebDriver driver;
	
	@Before
	public void Setup() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
	}
	
	@Test
	public void test() throws InterruptedException {
		
		//1. Go to: http://ironsummitmedia.github.io/startbootstrap-agency/
		driver.get(" http://ironsummitmedia.github.io/startbootstrap-agency/");
		Thread.sleep(3000);
		//2. Click on menu "TEAM"
		driver.findElement(By.cssSelector("a[href^=\"#team\"]")).click();
		Thread.sleep(3000);
		//3. Verify Kay Garland's avatar is present.
		if(driver.findElement(By.cssSelector("div.team-member > img")).isDisplayed()) {			
			System.out.println("Displayed");
			}
		else {
			System.out.println("Not displayed");
			}
		Thread.sleep(5000);
		//4. Click on menu "CONTACT"
		driver.findElement(By.cssSelector("a[href^=\"#contact\"]")).click();
		Thread.sleep(2000);
		//5. Enter "YOUR NAME", "YOUR EMAIL", "YOUR PHONE", "YOUR MESSAGE".
		driver.findElement(By.cssSelector("#name")).sendKeys("Nguyen Ngan");
		driver.findElement(By.cssSelector("#email")).sendKeys("ngan@gmail.com");
		driver.findElement(By.cssSelector("#phone")).sendKeys("0989768564");
		driver.findElement(By.cssSelector("#message")).sendKeys("abc");
		Thread.sleep(8000);
		//6. EXIT
		driver.close();
	}
		/*
		driver.findElement(By.cssSelector("")).getText().Contains(_active);
		WebElement element = driver.findElement(By.cssSelector(""));
        String path = element.getAttribute("src");
		
		/*
			WebElement ImageFile = driver.findElement(By.cssSelector("#team > div > div:nth-child(2) > div:nth-child(1) > div > img"));
		        
		        Boolean ImagePresent = (Boolean) ((JavascriptExecutor)driver).executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", ImageFile);
		        if (!ImagePresent)
		        {
		             System.out.println("Image not displayed.");
		        }
		        else
		        {
		            System.out.println("Image displayed.");
		        }  */
	
	@After
	public void tearDown() {
		driver.quit();
	}
}
