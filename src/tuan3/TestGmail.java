package tuan3;


import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;


public class TestGmail {
  private WebDriver driver;
  private String baseUrl;

@BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://accounts.google.com/SignUp";
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void test() throws Exception {
    driver.get(baseUrl + "");
    //check title
    driver.findElement(By.id("LastName")).sendKeys("Trang");
    driver.findElement(By.id("FirstName")).sendKeys("Vu");
    driver.findElement(By.id("GmailAddress")).sendKeys("abc@gmail.com");
    driver.findElement(By.id("Passwd")).sendKeys("123");
    driver.findElement(By.id("PasswdAgain")).sendKeys("123");
    driver.findElement(By.id("birthday-placeholder")).sendKeys("19");
    //Select month = new Select (driver.findElement(By.id(":0")));
    //month.selectByIndex(0);
    driver.findElement(By.id(":0")).click();
    driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/div/form/div[5]/fieldset/label[2]/span/div[2]/div[1]/div")).click();
    
    driver.findElement(By.id("birthyear-placeholder")).sendKeys("1994");
   // Select gender = new Select (driver.findElement(By.id("Gender")));
   // gender.selectByIndex(1);
    driver.findElement(By.id("RecoveryPhoneNumber")).sendKeys("972722341");
    driver.findElement(By.id("RecoveryEmailAddress")).sendKeys("no@gmail.com");
    System.out.println("Enter the capcha and wait for 10 seconnds");    
    Thread.sleep(10000);
   // Select nation = new Select ((driver.findElement(By.id(":h"))));
   // nation.selectByIndex(12);
    driver.findElement(By.id("TermsOfService")).click();
  }


@AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
	  //close browser
    driver.quit();

  }

}
