package tuan6;

//Css Selector
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestReverseCss {
	private WebDriver driver ;
	private String baseUrl;
	
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "http://www.reverso.net/";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.MILLISECONDS);
	}

	@Test
	public void test() throws InterruptedException {
		driver.get(baseUrl + "spell-checker/english-spelling-grammar/");
		driver.findElement(By.cssSelector("textarea#startText")).sendKeys("hello, my name are John");
		driver.findElement(By.cssSelector("a#btnSpell")).click();
		Thread.sleep(15000);
		//click logo
		driver.findElement(By.cssSelector("#reversoLogo > a > img")).click();
		Thread.sleep(10000);
		//driver.findElement(By.linkText("Dictionary")).click();
		driver.findElement(By.cssSelector("a.mItemN[href='http://dictionary.reverso.net'] > span")).click();
		// type book
		driver.findElement(By.cssSelector("input#ctl00_cC_txtSearchText")).sendKeys("book");
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("a#ctl00_cC_lnkSearch.search_btn")).click();
		Assert.assertEquals(driver.getCurrentUrl(),"http://dictionary.reverso.net/english-definition/");
	}
	
		
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
	
}
