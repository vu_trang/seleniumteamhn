package tuan5;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
//test CSS

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.firefox.FirefoxDriver;

public class TestReverse {
	private WebDriver driver ;
	private String baseUrl;
	
	
	@BeforeMethod
	public void setUp() {
		driver = new FirefoxDriver();
		baseUrl = "http://www.reverso.net/spell-checker/english-spelling-grammar/";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.MILLISECONDS);
	}

	@Test
	public void test() {
		driver.get(baseUrl + "");
		driver.findElement(By.cssSelector("textarea#startText")).sendKeys("hello, my name are John");
		driver.findElement(By.cssSelector("a#btnSpell")).click();
	}
	
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
	
}
