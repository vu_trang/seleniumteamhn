package tuan2;


import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class VuThiTrang_bai3 {
  private WebDriver driver;
  private String baseUrl;
 
  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://ask.fm";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testVuThiTrangBai3() throws Exception {
    driver.get(baseUrl + "/signup");
    driver.findElement(By.id("user_login")).clear();
    driver.findElement(By.id("user_login")).sendKeys("TrangVt2");
    driver.findElement(By.id("user_name")).clear();
    driver.findElement(By.id("user_name")).sendKeys("Trang Vu");
    driver.findElement(By.id("user_password")).clear();
    driver.findElement(By.id("user_password")).sendKeys("Trang1912");
    driver.findElement(By.id("user_email")).clear();
    driver.findElement(By.id("user_email")).sendKeys("nhungnhithitcho@gmail.com");
    new Select(driver.findElement(By.id("date_day"))).selectByVisibleText("19");
    new Select(driver.findElement(By.id("date_month"))).selectByVisibleText("September");
    new Select(driver.findElement(By.id("date_year"))).selectByVisibleText("2005");
    driver.findElement(By.xpath("//input[@value='Sign up']")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
  }
}
