package tuan3;

import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestChrome {
  private WebDriver driver;
  private String baseUrl;

@BeforeClass(alwaysRun = true)
  public void setUp() throws Exception { 
    baseUrl = "https://translate.google.com";
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void test() throws Exception {
	System.setProperty("webdriver.chrome.driver", "E:/Selenium/lib/chromedriver_win32");
    WebDriver driver = new ChromeDriver();              
    driver.get(baseUrl + "/");
    //check title
    driver.findElement(By.id("source")).sendKeys("bonjour!");
    driver.findElement(By.id("gt-submit")).click();
  }


@AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
	  //close browser
    driver.quit();

  }

}
